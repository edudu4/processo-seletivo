# processo-seletivo
Funcionalidades de Create, Update e Delete adicionadas ao projeto, utilizando Jersey(JAX-RS).

# Requisitos

1. Postman
2. Maven
3. IDE (Eclipse) 

# Passo a Passo
1. Faça um clone desse projeto.
2. Use o comando -mvn dependency:resolve
3. Configure o Persistence.xml com o usuário e senha do banco de dados utilizado.
4. Rode o projeto e faça as requisições pelo postman utilizando a url: "localhost:8080/mercado/rs/produtos/" (use as funções "Get", "Post", "Put" e "Delete" para todas as funcionalidades implementadas.)
